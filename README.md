[travis-repo]: https://travis-ci.org/hpedrorodrigues/Tyr-server
[travis-badge]: https://travis-ci.org/hpedrorodrigues/Tyr-server.svg?branch=master

# Tyr Server

[![Build Status][travis-badge]][travis-repo]

A utility app with some features to help you to handle JSON.

Client side of this project is [here][tyr-client].

This project uses [Hyperion][hyperion-url].

## API Documentation

Documentation [here][docs-url].

## License

This project is released under the MIT license. See [LICENSE](./LICENSE) for details.

## More

This project is a work in progress, feel free to improve it.


[hyperion-url]: https://github.com/hpedrorodrigues/Hyperion
[docs-url]: http://docs.jsonserver.apiary.io/#
[tyr-client]: https://github.com/hpedrorodrigues/Tyr