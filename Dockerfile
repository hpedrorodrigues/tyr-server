FROM openjdk:8

MAINTAINER Pedro Rodrigues <hs.pedro.rodrigues@gmail.com>

RUN apt-get update

WORKDIR /opt/tyr-server

COPY target/tyr-server-*.jar /opt/tyr-server/tyr-server.jar

ENTRYPOINT ["java","-jar","/opt/tyr-server/tyr-server.jar"]