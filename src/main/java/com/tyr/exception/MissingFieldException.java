package com.tyr.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class MissingFieldException extends GenericRuntimeException {

    private static final long serialVersionUID = 941032728805477458L;

    public MissingFieldException(final String fieldName) {
        super("Missing field with name " + fieldName + ".");
    }
}