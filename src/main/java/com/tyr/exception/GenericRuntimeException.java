package com.tyr.exception;

public class GenericRuntimeException extends RuntimeException {

    private static final long serialVersionUID = 8867273202418892290L;

    public GenericRuntimeException(String message) {
        super(message);
    }
}