package com.tyr.service;

import com.hpedrorodrigues.hyperion.Hyperion;
import com.hpedrorodrigues.hyperion.configuration.Metadata;
import com.hpedrorodrigues.hyperion.model.PlainEntity;
import com.tyr.dto.Request;
import com.tyr.exception.MissingFieldException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ParserService extends AbstractService {

    public List<PlainEntity> parse(final Request code) {
        if (code.getLanguage() == null) {

            throw new MissingFieldException("language");
        } else if (code.getJson() == null) {

            throw new MissingFieldException("json");
        } else {

            final Metadata metadata = new Metadata();

            metadata.setRootEntityName(code.getEntityName());
            metadata.setNamespace(code.getPackageName());
            metadata.setLanguage(code.getLanguage());
            metadata.setJson(code.getJsonAsText());

            return Hyperion.parse(metadata);
        }
    }
}