package com.tyr.controller;

import com.hpedrorodrigues.hyperion.model.PlainEntity;
import com.tyr.dto.Request;
import com.tyr.service.ParserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class ApiController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApiController.class);

    @Autowired
    private ParserService parserService;

    @RequestMapping(value = "/parse", method = RequestMethod.POST)
    public List<PlainEntity> parse(@RequestBody Request code) {
        LOGGER.info("Received data: [{}]", code);
        return parserService.parse(code);
    }
}