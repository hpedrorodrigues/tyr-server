package com.tyr.dto;

import com.fasterxml.jackson.databind.JsonNode;
import com.hpedrorodrigues.hyperion.model.enumeration.Language;
import lombok.Data;

@Data
public class Request {

    private Language language;

    private String entityName;

    private String packageName;

    private JsonNode json;

    public String getJsonAsText() {
        return String.valueOf(json);
    }
}