#!/bin/bash

source configuration.sh

for port in "${ports[@]}"; do

  echo "Port forwarding: $port"
  VBoxManage modifyvm "$machine_name" --natpf1 "tcp-port$port,tcp,,$port,,$port";
done