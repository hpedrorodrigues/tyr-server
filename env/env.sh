#!/bin/bash

source configuration.sh

if [[ "$OSTYPE" == "darwin"* ]]; then

  machine_env=`docker-machine env ${machine_name}`

  if [ -z "${machine_env##*"is not running"*}" ]; then
    echo "This machine is not running"
    docker-machine start ${machine_name}

  elif [ -z "${machine_env##*"wrong running"*}" ];then
    echo "This machine is in a wrong running"
    docker-machine restart ${machine_name}
  fi

  eval $(docker-machine env ${machine_name})

  echo
  echo "Starting containers..."
  echo

  docker-compose up --build -d
else

  echo
  echo "Starting containers..."
  echo

  docker-compose up -d
fi
