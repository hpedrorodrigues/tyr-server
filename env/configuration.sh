#!/usr/bin/env bash

machine_name="default"

# Port forwards only TCP ports
ports=(5432 8080)
